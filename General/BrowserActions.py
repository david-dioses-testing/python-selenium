from selenium import webdriver
from selenium.webdriver import ActionChains

from selenium.common.exceptions import NoSuchElementException


def check_exists_by_xpath(context, xpath):
    try:
        context.driver.find_element_by_xpath(xpath)
    except NoSuchElementException:
        return False
    return True


def assert_exist_by_xpath(context, xpath):
    assert check_exists_by_xpath(context, xpath)


def page_has_loaded(context):
    page_state = context.driver.execute_script(
        'return document.readyState;'
    )
    return page_state == 'complete'


def start_driver(context):
    browser = context.config.get("Browser", "browser");
    driver_folder = '.\\drivers\\'
    if browser == "chrome":
        context.driver = webdriver.Chrome(driver_folder + 'chromedriver.exe')
    context.driver.implicitly_wait(context.config.get("Driver", "implicitly_wait"))  # seconds


def go_to_url(context, url):
    context.driver.get(url)
    page_has_loaded(context)


def getElementByXpath(context, xpath):
    return context.driver.find_element_by_xpath(xpath)


def hoverOverAndClick(context, element1, element2):
    Hover = ActionChains(context.driver).move_to_element(element1).move_to_element(element2)
    Hover.click().perform()
