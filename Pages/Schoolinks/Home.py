from General.BrowserActions import *


def navigate_to_homepage(context):
    go_to_url(context, "https://www.schoolinks.com/")


def select_footer_option(context, option_name, suboption_name):
    option_element = getElementByXpath(context, "//footer//li[.//text()[normalize-space(.)='" + option_name + "']]")
    suboption_element = getElementByXpath(context,
                                          "//footer//div[@class='dropdown-content']//a[.='" + suboption_name + "']")
    hoverOverAndClick(context, option_element, suboption_element)
