from General.BrowserActions import *


def validate_title_in_for_school(context):
    assert_exist_by_xpath(context,
                          "//span[contains(@class,'cover__title_text')][.='Private or public, small or big, we have a solution for your school.']")
