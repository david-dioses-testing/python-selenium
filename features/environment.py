import configparser
from behave.log_capture import capture


@capture
def before_all(context):
    context.config = configparser.ConfigParser()
    context.config.read('Config.properties')
