from behave import *

from Pages.Schoolinks.Home import *
from Pages.Schoolinks.ForSchools import *


@given('I navigate to schoolinks page')
def navigate_to_schoolinks(context):
    navigate_to_homepage(context)


@when('I go to footer option: {option} > {suboption}')
def go_to_footer_option(context, option, suboption):
    select_footer_option(context, option, suboption)


@Then('I validate title in For School Page')
def validate_title_in_For_School_Page(context):
    validate_title_in_for_school(context)
